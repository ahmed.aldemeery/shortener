<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use phpDocumentor\Reflection\Types\This;

class Url extends Model
{
    use HasFactory;

    protected $fillable = [
        'value',
        'handle',
    ];

    public function user() {
       return $this->belongsTo(User::class);
    }
}
