<?php

namespace App\Http\Controllers;

use App\Models\Url;
use Illuminate\Http\Request;

class UrlController extends Controller
{
    public function create()
    {
        return view('urls/create');
    }

    public function store(Request $request)
    {
        $data = $request->validate([
            'value' => 'required|url',
        ]);

        $data['handle'] = uniqid();

        Url::create($data);

        return back()->with([
            'url' => route('urls.redirect', ['handle' => $data['handle']])
        ]);
    }

    public function redirect(String $handle)
    {
        $url = Url::where('handle', '=', $handle)->firstOrFail();

        return redirect($url->value);
    }

    public function view()
    {
        return view('urls/view');
    }
}
