<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Create Page</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/water.css@2/out/dark.css">
</head>
<body>
    <h1>Create</h1>
    <a href="{{ route('logout') }}">Logout |</a>
    <a href="{{ route('urls.view') }}">View</a>
    <hr>
    <form action="{{ route('urls.store') }}" method="POST">
    @csrf

    <label for="url">Url</label>
    <input type="Text" name="value" id="url">

    <input type="submit" value="Shrink">

    {{-- @if(session('url'))
        {{  session("url") }}
    @endif --}}

    </form>
</body>
</html>
