<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Register Page</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/water.css@2/out/dark.css">
</head>
<body>
    <h1>Register</h1>
    <a href="{{ route('login') }}">Login</a>
    <hr>
    <form action="{{ route('signup') }}" method="POST">
    @csrf

    <label for="name">Name</label>
    <input type="text" name="name" id="name">

    <label for="email">Email</label>
    <input type="email" name="email" id="email">

    <label for="password">Password</label>
    <input type="password" name="password" id="password">

    <label for="passwprd_confirmation">Password Confirmation</label>
    <input type="password" name="password_confirmation" id="password_confirmation">

    <input type="submit" value="Register">

    </form>
</body>
</html>
