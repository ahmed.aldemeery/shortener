<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UrlController;
use App\Http\Controllers\AuthController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::group(['middleware' => 'guest'], function () {
    Route::get('login', [AuthController::class, 'login'])->name('login');
    Route::get('register', [AuthController::class, 'register'])->name('register');
    Route::post('signup', [AuthController::class, 'signup'])->name('signup');
    Route::post('signin', [AuthController::class, 'signin'])->name('signin');

});
Route::group(['middleware' => 'auth'], function () {

    Route::get('create', [UrlController::class, 'create'])->name('urls.create');
    Route::post('urls', [UrlController::class, 'store'])->name('urls.store');
    Route::get('view', [UrlController::class, 'view'])->name('urls.view');
    Route::get('logout', [AuthController::class, 'logout'])->name('logout');
    Route::get('{handle}', [UrlController::class, 'redirect'])->name('urls.redirect');
});

